package com.miniprj.interviewcode;


//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = { WebMvcConfig.class, WebSecurityConfig.class })
class InterviewcodeApplicationTests {

//	@Autowired
//	private MinisterResource ministerResource;
//
//	@MockBean
//	private MinisterClient ministerClient;
//
//	private List<Minister> listMinister;
//
//	@Autowired
//	private RestTemplate restTemplate;
//
//	@Before
//	public void setUp() throws Exception {
//		MockitoAnnotations.initMocks(this);
//
//	}
//
//	@Autowired
//	private MockMvc mockMvc;
//
//	@Test
//	public void helloworld() throws InterruptedException, ExecutionException {
//		CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> "Hello")
//				.thenCompose(s -> CompletableFuture.supplyAsync(() -> s + " World"));
//
//		assertEquals("Hello World", completableFuture.get());
//	}
//
//	@Test
//	public void getMinisterList() throws Exception {
////		Minister minister = Minister.builder().dob("Minister of Education").identityAddress("Jakarta")
////				.lastEducation("S2").name("Nadiem Makarin").nik(555).occupation("WNI").build();
////
////		listMinister = new ArrayList<Minister>();
////		listMinister.add(minister);
////
////		MinisterClient ministerClientMock = mock(MinisterClient.class);
////		when(ministerClientMock.getList()).thenReturn(CompletableFuture.completedFuture(listMinister));
////		MinisterClient businessImpl = new MinisterClient();
////		CompletableFuture<List<Minister>> result = businessImpl.getList();
////
////		assertEquals(listMinister.get(0).getIdentityAddress(), result.get().get(0).getIdentityAddress());
//		
//		Minister mn = restTemplate.getForObject("http://localhost:9090/minister/list", Minister.class);
//		assertNotNull(mn);
//		
//	}
//
//	@Test
//	public void testAddMinisterMissingHeader() throws URISyntaxException {
//
//		final String baseUrl = "http://localhost:9090/minister/add";
//
//		URI uri = new URI(baseUrl);
//
//		Minister ministerObj = new Minister("Java", "Bandung", "S1", "Argana", 10, "WNI");
//
//		HttpHeaders headers = new HttpHeaders();
//
//		HttpEntity<Minister> request = new HttpEntity<>(ministerObj, headers);
//
//		System.out.println("=============" + ministerObj);
//		try {
//			restTemplate.postForEntity(uri, request, String.class);
//			Assert.fail();
//		} catch (HttpClientErrorException ex) {
//			// Verify bad request and missing header
//			Assert.assertEquals(400, ex.getRawStatusCode());
//			Assert.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
//		}
//	}
//
//	@Test
//	public void testEditMinisterMissingHeader() throws URISyntaxException {
//
//		int nik = 5;
//		final String baseUrl = "http://localhost:9090/minister/add" + nik;
//
//		URI uri = new URI(baseUrl);
//
//		Minister ministerObj = new Minister();
//		ministerObj.setDob("Dodol garut");
//
//		HttpHeaders headers = new HttpHeaders();
//
//		HttpEntity<Minister> request = new HttpEntity<>(ministerObj, headers);
//
//		try {
//			restTemplate.postForEntity(uri, request, String.class);
//			Assert.fail();
//		} catch (HttpClientErrorException ex) {
//			// Verify bad request and missing header
//			Assert.assertEquals(400, ex.getRawStatusCode());
//			Assert.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
//		}
//	}
//
//	@Test
//	public void testDeleteMinisterMissingHeader() throws URISyntaxException {
//
//		int nik = 3;
//		final String baseUrl = "http://localhost:9090/minister/delete?nik=" + nik;
//
//		URI uri = new URI(baseUrl);
//
//		Minister ministerObj = new Minister();
//		ministerObj.setDob("Dodol garut");
//
//		HttpHeaders headers = new HttpHeaders();
//
//		HttpEntity<Minister> request = new HttpEntity<>(ministerObj, headers);
//
//		try {
//			restTemplate.delete(uri);
//			Assert.fail();
//		} catch (HttpClientErrorException ex) {
//			// Verify bad request and missing header
//			Assert.assertEquals(400, ex.getRawStatusCode());
//			Assert.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
//		}
//	}

}
