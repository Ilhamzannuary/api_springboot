package com.miniprj.interviewcode.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.miniprj.interviewcode.model.account.Account;
import com.miniprj.interviewcode.model.role.Role;
import com.miniprj.interviewcode.model.role.RoleName;
import com.miniprj.interviewcode.model.user.User;




@Repository
public interface IAccountRepository extends JpaRepository<Account, Long>{
	
	
	@Query(value = "SELECT * FROM m_account WHERE account_id = :idAccount", nativeQuery = true)
	public List<Account> getListByIdUser(@Param("idAccount") String accountId);
		
	public Account save(Account account);
	

	    
	
}
