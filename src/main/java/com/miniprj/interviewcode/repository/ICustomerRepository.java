package com.miniprj.interviewcode.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.miniprj.interviewcode.model.account.Account;
import com.miniprj.interviewcode.model.customer.Customer;
import com.miniprj.interviewcode.model.role.Role;
import com.miniprj.interviewcode.model.role.RoleName;




@Repository
public interface ICustomerRepository extends JpaRepository<Customer, Long>{
	
	
	    
	
}
