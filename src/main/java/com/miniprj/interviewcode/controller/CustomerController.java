package com.miniprj.interviewcode.controller;

import java.math.BigInteger;
import java.net.URI;
import java.time.Instant;
import java.util.Base64;
import java.util.Collections;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.miniprj.interviewcode.auth.sercurity.CurrentUser;
import com.miniprj.interviewcode.auth.sercurity.UserPrincipal;
import com.miniprj.interviewcode.model.account.Account;
import com.miniprj.interviewcode.model.customer.CreateCustomerRequest;
import com.miniprj.interviewcode.model.customer.Customer;
import com.miniprj.interviewcode.model.payload.ApiResponse;
import com.miniprj.interviewcode.repository.IAccountRepository;
import com.miniprj.interviewcode.repository.ICustomerRepository;

@RestController
@RequestMapping("/api")
public class CustomerController {
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IAccountRepository accountRepository;
	
	@Autowired
	ICustomerRepository customerRepository;
	
	
	private static String decode(String encodedString) {
	    return new String(Base64.getUrlDecoder().decode(encodedString));
	}
	
	
	public HttpHeaders SetCspHeader() {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Content-Security-Policy", "base-uri 'self'; object-src 'none'; script-src 'self'");
		return responseHeaders;
	}
	
	
//	@PreAuthorize("hasRole('USER')")
	@PostMapping("/customer/create")
	public ResponseEntity<?> createCustomerBank(@Valid @RequestBody CreateCustomerRequest request, @CurrentUser UserPrincipal currentUser,  HttpServletRequest httpRequest)throws Exception {
		
		// Must be called from request filtered by Spring Security, otherwise SecurityContextHolder is not updated
	    
	     
		 String token = httpRequest.getHeader("Authorization").substring(7);
		 String[] parts = token.split("\\.");
		 JSONObject payload = new JSONObject(decode(parts[1]));
		 boolean exp = payload.getLong("exp") > (System.currentTimeMillis() / 1000);
			if (exp) {
						Instant instant = Instant.now();
						String generateUUIDNo = String.format("%010d",new BigInteger(UUID.randomUUID().toString().replace("-",""),16));
						String unique_no = generateUUIDNo.substring( generateUUIDNo.length() - 10);
						//		Account account = new Account(unique_no, 0.00);

						//		accountRepository.save(account);
		
						Customer customer = new Customer(request.getTitle(), request.getUsername(), request.getEmailAddress(), request.getFirstName(), request.getLastName(), request.getNoTelp(), request.getAge(), instant);
		
						customerRepository.save(customer);
		
		
						return new ResponseEntity<>("Success", SetCspHeader(), HttpStatus.OK);
		
			} else {
				return new ResponseEntity<>("Token Expired...!", SetCspHeader(), HttpStatus.BAD_REQUEST);
			}
		
	}
	
}
