package com.miniprj.interviewcode.model.account;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.miniprj.interviewcode.model.customer.account.CustomerAccount;
import com.miniprj.interviewcode.model.role.Role;

@Entity
@Table(name = "m_account")
@NamedQuery(name = "Account.findAll", query = "SELECT r FROM Account r")
public class Account extends DateAudit {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    @NotBlank
	    @Column(name="account_id")
	    private String accountId;

	    @NotBlank
	    @Column(name="balance")
	    private double balance;

	    @OneToMany(mappedBy = "rAccount")
	    private List<CustomerAccount> customerAccount;
	    
	    public Account() {
	    	
	    }


		public Account(@NotBlank String accountId, @NotBlank double balance) {
			super();
			this.accountId = accountId;
			this.balance = balance;
		}

		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getAccountId() {
			return accountId;
		}


		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}


		public double getBalance() {
			return balance;
		}


		public void setBalance(double balance) {
			this.balance = balance;
		}


		public List<CustomerAccount> getCustomerAccount() {
			return customerAccount;
		}


		public void setCustomerAccount(List<CustomerAccount> customerAccount) {
			this.customerAccount = customerAccount;
		}

		
	    
	    
	    
	    
	
	
}