package com.miniprj.interviewcode.model.customer;

import javax.validation.constraints.NotBlank;


public class CreateCustomerRequest {
	 	@NotBlank
	    private String title;

	    @NotBlank
	    private String username;
	      
	    @NotBlank
	    private String emailAddress;
	    
	    @NotBlank
	    private String firstName;
	    
	    @NotBlank
	    private String lastName;
	    
	    @NotBlank
	    private String noTelp;
	    
	    @NotBlank
	    private String age;

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmailAddress() {
			return emailAddress;
		}

		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getNoTelp() {
			return noTelp;
		}

		public void setNoTelp(String noTelp) {
			this.noTelp = noTelp;
		}

		public String getAge() {
			return age;
		}

		public void setAge(String age) {
			this.age = age;
		}
	    
}
