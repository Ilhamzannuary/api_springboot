package com.miniprj.interviewcode.model.customer.account;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.miniprj.interviewcode.model.account.Account;
import com.miniprj.interviewcode.model.customer.Customer;
import com.miniprj.interviewcode.model.role.Role;
import com.miniprj.interviewcode.model.user.DateAudit;

@Entity
@Table(name = "r_customer_account")
public class CustomerAccount extends DateAudit {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    @NotBlank
	    @Column(name="enabled")
	    private boolean enabled;
	    
	    
	    @ManyToOne
	    private Customer rCustomer;
	    
	    @ManyToOne
	    private Account rAccount;

		public CustomerAccount(Long id, @NotBlank boolean enabled) {
			super();
			this.id = id;
			this.enabled = enabled;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public Customer getrCustomer() {
			return rCustomer;
		}

		public void setrCustomer(Customer rCustomer) {
			this.rCustomer = rCustomer;
		}

		public Account getrAccount() {
			return rAccount;
		}

		public void setrAccount(Account rAccount) {
			this.rAccount = rAccount;
		}

	   

	    
	    
	    
	    
	
	
}