package com.miniprj.interviewcode.model.customer;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.miniprj.interviewcode.model.account.Account;
import com.miniprj.interviewcode.model.customer.account.CustomerAccount;
import com.miniprj.interviewcode.model.role.Role;

@Entity
@Table(name = "m_customer", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
		@UniqueConstraint(columnNames = { "email_address" }) })
public class Customer extends DateAudit {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
	    @NotBlank
	    @Size(max = 4)
	    @Column(name="title")
	    private String title;

	    @NotBlank
	    @Column(name="username")
	    private String username;

	    @NaturalId
	    @NotBlank
	    @Size(max = 40)
	    @Email
	    private String email_address;

	    @NotBlank
	    @Size(max = 50)
	    @Column(name="first_name")
	    private String firstName;
	    
	    @NotBlank
	    @Size(max = 50)
	    @Column(name="last_name")
	    private String lastName;
	    
	    @NotBlank
	    @Size(max = 15)
	    @Column(name="no_telp")
	    private String noTelp;
	 
	    @NotBlank
	    @Size(max = 2)
	    @Column(name="age")
	    private String age;
		
		@Column(name="created_at")
		private Instant createdAt;
		
		@OneToMany(mappedBy = "rCustomer")
		private List<CustomerAccount> customerAccount;

	    public Customer() {
	    	
	    }

		public Customer(@NotBlank @Size(max = 4) String title, @NotBlank String username,
				@NotBlank @Size(max = 40) @Email String email_address, @NotBlank @Size(max = 50) String firstName,
				@NotBlank @Size(max = 50) String lastName, @NotBlank @Size(max = 15) String noTelp,
				@NotBlank @Size(max = 2) String age, Instant createdAt) {
			super();
			this.title = title;
			this.username = username;
			this.email_address = email_address;
			this.firstName = firstName;
			this.lastName = lastName;
			this.noTelp = noTelp;
			this.age = age;
			this.createdAt = createdAt;
			
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getEmail_address() {
			return email_address;
		}

		public void setEmail_address(String email_address) {
			this.email_address = email_address;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getNoTelp() {
			return noTelp;
		}

		public void setNoTelp(String noTelp) {
			this.noTelp = noTelp;
		}

		public String getAge() {
			return age;
		}

		public void setAge(String age) {
			this.age = age;
		}

		public Instant getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Instant createdAt) {
			this.createdAt = createdAt;
		}

		public List<CustomerAccount> getCustomerAccount() {
			return customerAccount;
		}

		public void setCustomerAccount(List<CustomerAccount> customerAccount) {
			this.customerAccount = customerAccount;
		}

		
		
		

	    
	    
	    
	
	
}