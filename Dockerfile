FROM openjdk:8
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ./target/*.jar api_springboot.jar
EXPOSE 8989
ENTRYPOINT ["java","-jar","/api_springboot.jar"]